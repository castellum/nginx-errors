FROM golang:alpine as builder
COPY main.go .
RUN go build -ldflags "-s -w" -o /nginx-errors main.go

FROM scratch
COPY www/ /www
COPY --from=builder /nginx-errors /
CMD ["/nginx-errors"]
