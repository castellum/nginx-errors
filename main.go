package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
)

func handler(w http.ResponseWriter, r *http.Request) {
	scode := r.Header.Get("X-Code")
	code, err := strconv.Atoi(scode)
	if err != nil {
		log.Printf("unexpected error reading return code: %v", err)
		http.NotFound(w, r)
		return
	}

	file := fmt.Sprintf("/www/%v.html", code)
	f, err := os.Open(file)
	if err != nil {
		log.Printf("unexpected error opening file: %v", err)
		http.NotFound(w, r)
		return
	}
	defer f.Close()

	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(code)
	io.Copy(w, f)
}

func dummy(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func main() {
	http.HandleFunc("/", handler)
	http.HandleFunc("/healthz", dummy)
	http.ListenAndServe(":8080", nil)
}
